FROM debian:stable-slim

RUN apt-get -y update && \
    echo '[ ! -z "$TERM" -a -r /etc/motd ] && cat /etc/motd' \
    >> /etc/bash.bashrc \
    ; echo "\
      _\n\
     /(|\n\
    (  :\n\
   __\  \  _____\n\
 (____)  \`|\n\
(____)|   |    JobTech Base Image - debian:stable-slim\n\
 (____).__|\n\
    (___)__.|_____\n\
\n" > /etc/motd
