
The current tag to use
```
jobtech-debian-slim-stable-base-image/jobtech-debian-slim-stable-base-image:2023011`
```
For example
```
FROM jobtech-debian-slim-stable-base-image/jobtech-debian-slim-stable-base-image:20230113 AS base
```
